-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 07 Novembre 2015 à 10:59
-- Version du serveur :  5.6.27-0ubuntu0.15.04.1
-- Version de PHP :  5.6.4-4ubuntu6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `medianet`
--

-- --------------------------------------------------------

--
-- Structure de la table `Adherent`
--

CREATE TABLE IF NOT EXISTS `Adherent` (
`Id` int(10) NOT NULL,
  `Nom` varchar(255) NOT NULL,
  `Prenom` varchar(255) NOT NULL,
  `Adresse` varchar(255) NOT NULL,
  `Telephone` varchar(255) DEFAULT NULL,
  `Mail` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Adherent`
--

INSERT INTO `Adherent` (`Id`, `Nom`, `Prenom`, `Adresse`, `Telephone`, `Mail`) VALUES
(1, 'Grosio', 'Michael', '15 rue general Hulot', '0777253649', 'michael.grosio@outlook.fr'),
(2, 'Spaeter', 'Julien', '54 avenue jean jaures\r\n', '0771088194', 'spaeterjulien@outlookfr'),
(3, 'Wallian', 'Raphael', '6 rue jean giraudoux', NULL, NULL),
(4, 'Khorchi', 'Johan', '16 rue des jardiniers', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `Document`
--

CREATE TABLE IF NOT EXISTS `Document` (
`Id` int(10) NOT NULL,
  `Titre` varchar(255) DEFAULT NULL,
  `Auteur` varchar(255) DEFAULT NULL,
  `Date` year(4) DEFAULT NULL,
  `Synopsis` text,
  `Etat` varchar(255) NOT NULL,
  `Image` varchar(255) DEFAULT NULL,
  `TypeId` int(10) NOT NULL,
  `GenreId` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Document`
--

INSERT INTO `Document` (`Id`, `Titre`, `Auteur`, `Date`, `Synopsis`, `Etat`, `Image`, `TypeId`, `GenreId`) VALUES
(1, 'Le Seigneur des anneaux', 'J.R.R. Tolkien', 1954, 'Après un long prologue décrivant les Hobbits et leurs mœurs, le passé de la Terre du Milieu et un rapide résumé des aventures de Bilbo Bessac, le livre I s''ouvre sur le cent onzième anniversaire de ce dernier, soixante années après les événements décrits dans Le Hobbit. Au cours de la réception, Bilbo s''éclipse grâce à l''invisibilité que lui confère son anneau magique et quitte Hobbiteville, laissant tous ses biens, anneau compris, à son neveu et héritier désigné, Frodo Bessac. Dix-sept ans plus tard, leur vieil ami, le magicien Gandalf le Gris, révèle à Frodo que son anneau est en réalité l''Anneau unique, instrument du pouvoir de Sauron', 'Disponible', 'mediaapp/ressources/images/seigneur1.jpg', 1, 3),
(2, 'Le Seigneur des anneaux II', 'J.R.R. Tolkien', 1954, 'Au début du livre III, Boromir meurt en tentant de défendre Merry et Pippin, qui sont enlevés par les Uruk-hai de Saruman. Après avoir offert des funérailles au capitaine du Gondor, Aragorn, Legolas et Gimli se lancent à leurs trousses à travers les plaines du Rohan. Aux abords de la forêt de Fangorn, ils retrouvent Gandalf, devenu Gandalf le Blanc et renvoyé en Terre du Milieu pour achever sa mission après avoir péri en terrassant le Balrog. il y a eu un film dessus.', 'Emprunté', 'mediaapp/ressources/images/seigneur2.jpg', 1, 3),
(3, 'Le Seigneur des anneaux III', 'J.R.R. Tolkien', 1955, 'Le livre V relate la lutte entre le Gondor et le Mordor, vue par Pippin à Minas Tirith et Merry aux côtés du roi Théoden du Rohan. La Cité Blanche, assiégée par des milliers d''Orques, est sauvée par l''arrivée des cavaliers de Rohan, puis par celle d''Aragorn à bord des navires d''Umbar, ce dernier ayant libéré le sud du Gondor grâce à l''armée des Morts. La bataille des champs du Pelennor se conclut par une défaite des forces de Sauron et par la mort de son plus puissant lieutenant, le Roi-Sorcier. Toutefois, les réserves dont dispose Sauron en Mordor sont largement supérieures en nombre à celles des Peuples Libres. ', 'Disponible', 'mediaapp/ressources/images/seigneur3.jpg', 1, 3),
(4, 'Ecliptica', 'Sonata Artica', 1999, 'Ecliptica est le premier album studio du groupe de power metal finlandais Sonata Arctica. L''album est sorti le 9 septembre 1999 sous le label Spinefarm Records en Europe et sous le label Century Media Records aux États-Unis.', 'Disponible', 'mediaapp/ressources/images/ecliptica.jpg', 2, 11),
(5, 'Random Access Memories', 'Daft Punk', 2013, 'Random Access Memories est le quatrième album studio de Daft Punk, sorti officiellement le 20 mai 2013n 1. Il est publié par Daft Life Limited, une filiale de Columbia Records. ', 'En réparation', 'mediaapp/ressources/images/random-access-memories.jpg', 2, 10),
(6, 'Jason Bourne', 'Robert Ludlum', 2002, 'Jason Bourne est le personnage de fiction, héros éponyme de la série littéraire créée par Robert Ludlum, poursuivie par Eric Van Lustbader et incarné par Matt Damon dans les adaptations cinématographiques. Il convient toutefois de noter que le Jason Bourne des romans est distinct du personnage des films.', 'Perdu', 'mediaapp/ressources/images/jason-bourne.jpg', 3, 2),
(7, 'Star Wars IV', 'George Lucas', 1977, 'La République galactique a été fondée pour amener la paix dans la galaxie, mais, tout au long de son existence, elle a été secouée par des sécessions et des guerres, notamment contre l''Empire Sith. Les chevaliers Jedi, gardiens de la paix et de la justice, réussissent à éliminer les Sith et la galaxie retrouve la prospéritéa 1,3. Mais après des millénaires d''existence, la République montre d''innombrables failles et se trouve fragilisée. Selon une prophétie Jedi, un « Élu » naîtra et rétablira un jour l''équilibre dans la Force.', 'Disponible', 'mediaapp/ressources/images/star-wars4.jpg', 3, 1),
(8, 'Le Seigneur des Anneaux', 'Peter Jackson', 2001, 'C''est le film du seigneur des anneaux: la communauté de l''anneau. Il est très bien mais un peu long.', 'Disponible', 'mediaapp/ressources/images/seigneurfilm.jpg', 3, 3),
(9, 'Narnia', 'Andrew Adamson', 2005, 'Le film Narnia n''est pas très interresant mais bon il en faut pour tout les goût. Ceci est un sinopsis.\r\n', 'Disponible', 'mediaapp/ressources/images/narnia1.jpg', 3, 3);

-- --------------------------------------------------------

--
-- Structure de la table `Emprunt`
--

CREATE TABLE IF NOT EXISTS `Emprunt` (
`Id` int(10) NOT NULL,
  `DateEmprunt` date NOT NULL,
  `DateRetour` date NOT NULL,
  `AdherentId` int(10) NOT NULL,
  `DocumentId` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Emprunt`
--

INSERT INTO `Emprunt` (`Id`, `DateEmprunt`, `DateRetour`, `AdherentId`, `DocumentId`) VALUES
(6, '2015-11-07', '2015-11-22', 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Genre`
--

CREATE TABLE IF NOT EXISTS `Genre` (
`Id` int(10) NOT NULL,
  `GenreDocument` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Genre`
--

INSERT INTO `Genre` (`Id`, `GenreDocument`) VALUES
(1, 'Aventure'),
(2, 'Action'),
(3, 'Fantastique'),
(4, 'Humoristique'),
(5, 'Horreur'),
(6, 'Science-fiction'),
(7, 'Historique'),
(8, 'Rock'),
(9, 'Rap'),
(10, 'Electro'),
(11, 'Metal');

-- --------------------------------------------------------

--
-- Structure de la table `Reservation`
--

CREATE TABLE IF NOT EXISTS `Reservation` (
`Id` int(10) NOT NULL,
  `DateReservation` date NOT NULL,
  `AdherentId` int(10) NOT NULL,
  `DocumentId` int(10) NOT NULL,
  `Etat` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Reservation`
--

INSERT INTO `Reservation` (`Id`, `DateReservation`, `AdherentId`, `DocumentId`, `Etat`) VALUES
(1, '2015-11-03', 1, 1, 'Pret'),
(2, '2015-11-02', 2, 4, 'En attente');

-- --------------------------------------------------------

--
-- Structure de la table `Type`
--

CREATE TABLE IF NOT EXISTS `Type` (
`Id` int(10) NOT NULL,
  `TypeMedia` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Type`
--

INSERT INTO `Type` (`Id`, `TypeMedia`) VALUES
(1, 'Livre'),
(2, 'CD'),
(3, 'DVD');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Adherent`
--
ALTER TABLE `Adherent`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `Id` (`Id`);

--
-- Index pour la table `Document`
--
ALTER TABLE `Document`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `Id` (`Id`), ADD KEY `FKDocument947752` (`TypeId`), ADD KEY `FKDocument34364` (`GenreId`);

--
-- Index pour la table `Emprunt`
--
ALTER TABLE `Emprunt`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `Id` (`Id`), ADD KEY `FKEmprunt83714` (`AdherentId`), ADD KEY `FKEmprunt662590` (`DocumentId`);

--
-- Index pour la table `Genre`
--
ALTER TABLE `Genre`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `Id` (`Id`);

--
-- Index pour la table `Reservation`
--
ALTER TABLE `Reservation`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `Id` (`Id`), ADD KEY `FKReservatio65287` (`AdherentId`), ADD KEY `FKReservatio290573` (`DocumentId`);

--
-- Index pour la table `Type`
--
ALTER TABLE `Type`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `Id` (`Id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Adherent`
--
ALTER TABLE `Adherent`
MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `Document`
--
ALTER TABLE `Document`
MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `Emprunt`
--
ALTER TABLE `Emprunt`
MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `Genre`
--
ALTER TABLE `Genre`
MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `Reservation`
--
ALTER TABLE `Reservation`
MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `Type`
--
ALTER TABLE `Type`
MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Document`
--
ALTER TABLE `Document`
ADD CONSTRAINT `FKDocument34364` FOREIGN KEY (`GenreId`) REFERENCES `Genre` (`Id`),
ADD CONSTRAINT `FKDocument947752` FOREIGN KEY (`TypeId`) REFERENCES `Type` (`Id`);

--
-- Contraintes pour la table `Emprunt`
--
ALTER TABLE `Emprunt`
ADD CONSTRAINT `FKEmprunt662590` FOREIGN KEY (`DocumentId`) REFERENCES `Document` (`Id`),
ADD CONSTRAINT `FKEmprunt83714` FOREIGN KEY (`AdherentId`) REFERENCES `Adherent` (`Id`);

--
-- Contraintes pour la table `Reservation`
--
ALTER TABLE `Reservation`
ADD CONSTRAINT `FKReservatio290573` FOREIGN KEY (`DocumentId`) REFERENCES `Document` (`Id`),
ADD CONSTRAINT `FKReservatio65287` FOREIGN KEY (`AdherentId`) REFERENCES `Adherent` (`Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
