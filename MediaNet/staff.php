<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 03/11/15
 * Time: 15:35
 */

require_once 'mediaapp/vendor/autoload.php';

app\App::DbConf('mediaapp/utils/config.ini');

if(isset($_POST)){
    switch (true)
    {
        case isset($_POST['btnsearch']):
            $search = $_POST['recherche'];
            $searchtype = $_POST['type'];
            $searchgenre = $_POST['genre'];

            $ctl = new controller\ControllerStaff();
            $ctl->recherche($search, $searchgenre, $searchtype);
            break;
        case isset($_POST['ajouterAdherent']):
            $ctl = new controller\ControllerStaff();
            $ctl->ajouterAdherent();
            break;
        case isset($_POST['ajouterEmprunt']):
            $adheId = $_POST['AdherentId'];
            $doc1 = $_POST['Document1'];
            $ctl = new controller\ControllerStaff();
            $ctl->ajouterEmprunt($adheId, $doc1);
            break;
        case isset($_POST['supprimerEmprunt']):
            $ctl = new controller\ControllerStaff();
            $ctl->supprimerEmprunt();
            break;
        case isset($_POST['changeEtat']):
            $etat = $_POST['etat'];
            $ctl = new controller\ControllerStaff();
            $ctl->changerEtatDocument($_GET['document'], $etat);
            break;
    }
}

if(isset($_GET['menu'])) {
    $search = $_GET['menu'];

    switch ($search) {
        case 'recherche':
            $ctl = new controller\ControllerStaff();
            $ctl->afficheRecherche();
            break;
        case 'documents':
            $ctl = new controller\ControllerStaff();
            $ctl->afficheDocument();
            break;
        case 'emprunts':
            $ctl = new controller\ControllerStaff();
            $ctl->afficherAjoutEmprunt();
            break;
        case 'retour':
            $ctl = new controller\ControllerStaff();
            $ctl->afficherSupprimerEmprunt();
            break;
        case 'adherent':
            $ctl = new controller\ControllerStaff();
            $ctl->formulaireAdherent();
            break;
        case 'resultat':
            $ctl = new controller\ControllerStaff();
            $ctl->afficherResultat($_GET['document']);
            break;
        case 'ajouterEmprunt':
            $ctl = new controller\ControllerStaff();
            $ctl->afficherAjoutEmprunt();
            break;
    }
}
else{
    $ctl = new controller\ControllerStaff();
    $ctl->afficheRecherche();
}
