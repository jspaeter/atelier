<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 06/11/15
 * Time: 08:26
 */

namespace view;


class Staff
{
    private $res, $type, $genre, $etat, $DocumentRetour, $DocumentEmprunt;

    //Getter
    public function __get($attr_name)
    {
        if (property_exists(__CLASS__, $attr_name)) {
            return $this->$attr_name;
        }
        $emess = __CLASS__ . ": unknown member $attr_name (__get)";
        throw new \Exception($emess);
    }

    //Setter
   public function __set($attr_name, $attr_val)
    {
        if (property_exists(__CLASS__, $attr_name))
            $this->$attr_name = $attr_val;
        else {
            $emess = __CLASS__ . ": unknown member $attr_name (__set)";
            throw new \Exception($emess);
        }
    }

    public function afficher($action)
    {
        $html = "<!DOCTYPE html>
                    <html lang='fr'>
                <head>
                    <meta charset='UTF-8'>
                    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                    <link rel='stylesheet' type='text/css' href='mediaapp/ressources/css/stylesheets/styleStaff.css'>
                    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
                    <script src='mediaapp/ressources/js/script.js'></script>

                    <title>MediaNet</title>
                </head>
                <body>

                <header>
                    <div class='icon' onClick='afficheMenu()'>
                        <div id='menu'>
                            <nav>
                                <ul>
                                    <li><a href='staff.php?menu=recherche'>Recherche</a></li>
                                    <li><a href='staff.php?menu=documents'>Voir les Documents</a></li>
                                    <li><a href='staff.php?menu=ajouterEmprunt'>Ajouter un Emprunt</a></li>
                                    <li><a href='staff.php?menu=retour'>Effectuer un Retour</a></li>
                                    <li><a href='staff.php?menu=adherent'>Gestion des Adhérents</a></li>
                                </ul>
                            </nav>
                        </div>
                        </div>
                    <h1 class='navtitre'>MediaNet</h1>
                </header>";

        switch ($action) {
            case 'afficheDocument':
                $html .= $this->afficheDocument();
                break;
            case 'recherche':
                $html .= $this->recherche();
                break;
            case 'resultats':
                $html .= $this->resultats();
                break;
            case 'formulaireAdherent':
                $html .= $this->ajouterAdherent();
                break;
            case 'ajouterEmprunt':
                $html .= $this->ajouterEmprunt();
                break;
            case 'supprimerEmprunt':
                $html .= $this->supprimerEmprunt();
                break;
            case 'retourEmprunt':
                $html .= $this->retourEmprunt();
                break;
            case 'page':
                $html .= $this->page();
                break;
        }

        $html .= "</body></html>";
        echo $html;
    }

    private function afficheDocument(){


        $resultat = "<section><h1>Documents</h1>";

        $resultat .= "<div class ='line'><p>Id</p><p>Titre</p><p>Auteur</p><p>Etat</p></div>";
        
        foreach ($this->res as $r) {

            $resultat .= "<div class = 'doc'>
                            <p>$r->Id</p>
                            <p>$r->Titre</p>
                            <p>$r->Auteur</p>";

            if ($r->Etat == 'Disponible') {
                $resultat .= "<p class='etat Vert'>$r->Etat</p>";
            }

            elseif ($r->Etat == 'Emprunté' || $r->Etat == 'Réservé') {
                $resultat .= "<p class='etat Orange'>$r->Etat</p>";
            }

            else {
                $resultat .= "<p class='etat Rouge'>$r->Etat</p>";
            }
            $resultat .= "<form method='post' action='staff.php?menu=changementEtat&document=$r->Id'> ";
            $resultat .= "<select name='etat'>";
            $resultat .= "<option value='Disponible'>Disponible</option >";
            $resultat .= "<option value='Perdu'>Perdu</option >";
            $resultat .= "<option value='En réparation'>En réparation</option >";
            $resultat .= "<option value='Réservé'>Réservé</option >";
            $resultat .= "</select>";
            $resultat .= "<button type='submit' name ='changeEtat' >Changer l'état</button>";
            $resultat .= "</form>";
            $resultat .= "</div>";

        }
        return $resultat;
    }

    private function recherche(){
        $recherche = "<section>
                            <h1>Recherche</h1>

                            <form method='post' action='staff.php?menu=resultatRecherche'>
                                <input type='search' name='recherche' value ='' placeholder='Recherche....'>
                                <select name='type'>
                                <option value='type'>Type</option>";

        foreach ($this->type as $t) {
            $recherche .= "<option value=" . $t->TypeMedia . "> " . $t->TypeMedia . "</option >";
        }

        $recherche .= "</select>";
        $recherche .= "<select name='genre'>
                           <option value='genre'>Genre</option>";

        foreach ($this->genre as $g) {
            $recherche .= "<option value=" . $g->GenreDocument . "> " . $g->GenreDocument . "</option >";
        }

        $recherche .= "</select>";

        $recherche .= "<button class ='btn' type='submit' name ='btnsearch'>Ok</button>
                            </form>
                        </section>";

        return $recherche;
    }

    private function resultats()
    {
        $resultat = "<section>

         <h1>Resultats</h1>";
        foreach ($this->res as $r) {
            $resultat .= "<div class = 'resultat'><a href='staff.php?menu=resultat&document=$r->Id' >
                             <h2>$r->Titre</h2>

                             <img src =' $r->Image'/>

                             <div class = 'description'>
                                 <p>Auteur : $r->Auteur</p>
                                 <p>Date de publication : $r->Date</p>
                                 <p>Synopsis : $r->Synopsis</p></div>";
            if ($r->Etat == 'Disponible') {
                $resultat .= "<div class='etat Vert'>Disponibilité : $r->Etat</div>";
            }
            elseif ($r->Etat == 'Emprunté' || $r->Etat == 'Réservé') {
                $resultat .= "<div class='etat Orange'>Disponibilité : $r->Etat</div>";
            }
            else {
                $resultat .= "<div class='etat Rouge'>Disponibilité : $r->Etat</div>";
            }
            $resultat .= "</div>

                 </div>";
        }

        $resultat .= "</section>";
        return $resultat;
    }

    private function ajouterAdherent()
    {
        $adherent = '<section><h1>Ajouter un Adhérent</h1>';
        $adherent .= '<form action="staff.php?menu=AjoutAdherent" method="post">
                        <div>
                            <label for="Nom">Nom :</label>
                            <input type="text" name="Nom" placeholder="Nom" required>

                            <label for="Prenom">Prénom :</label>
                            <input type="text" name="Prenom" placeholder="Prenom" required>

                            <label for="Adresse">Adresse :</label>
                            <input type="text" name="Adresse" placeholder="Adresse">

                            <label for="Telephone">Téléphone :</label>
                            <input type="number" name="Telephone" placeholder="Telephone">

                            <label for="Mail">Mail :</label>
                            <input type="email" name="Mail" placeholder="Mail">
                            <button class="btn" type="submit" name="ajouterAdherent">Valider</button>
                        </div>    
            </form></section>';

        return $adherent;
    }

    private function ajouterEmprunt()
    {
        $ajoutEmprunt = '<section><h1>Ajout emprunt</h1>';
        $ajoutEmprunt .= '<form class="ajouterEmprunt" action="staff.php?menu=emprunts" method="post">
                            <div>
                                <label for="AdherentId">Id adherent :</label>
                                <input type="number" id="AdherentId" name="AdherentId">

                                <label for="Document1">Document :</label>
                                <input type="number" id="Document1" name="Document1">

                                <button class="btn" type="submit" name="ajouterEmprunt">Valider</button>
                            </div>
                        </form>
                        </section>';

        return $ajoutEmprunt;
    }

    public function retourEmprunt()
    {
        $liste = $this->res;
        $supprEmprunt = '<section>
                        <h1>Retour emprunt</h1>
                <form action="staff.php?menu=suprEmprunt" method="post"><div>
                <label for="DocumentId">Id Document :</label>
                <input type="number" name="DocumentId">
                <button class="btn" type="submit" name="supprimerEmprunt">Valider</button>
            </div></form>';
        $supprEmprunt .= '<div class="deletedoc"> Document retourné :   '.$liste[1].' </div>
<div class = "listedoc">Liste des documents à rendre : ';
        foreach ($liste[0] as $l) {
            $supprEmprunt .= '<div>';
                $supprEmprunt .= "-".$l->Titre;
            $supprEmprunt .= '</div>';
        }
        $supprEmprunt .= '</div></section>';
        return $supprEmprunt;
    }

    private function supprimerEmprunt()
    {
        $supprEmprunt = '<section>
                        <h1>Retour emprunt</h1>
                <form action="staff.php?menu=suprEmprunt" method="post"><div>
                <label for="DocumentId">Id Document :</label>
                <input type="number" name="DocumentId">
                <button class="btn" type="submit" name="supprimerEmprunt">Valider</button>
            </div></form></section>';

        return $supprEmprunt;
    }

    private function page(){
        $page = "<section>";
        foreach($this->res as $r){
            $page .= "<h1>$r->Titre</h1>
                        <div class = 'document'>


                             <img src =' $r->Image'/>

                             <div class = 'element'>
                                 <p>Auteur : $r->Auteur</p>
                                 <p>Date de publication : $r->Date</p></div>
                             <div class = 'synopsis'>
                                 <p>Synopsis : $r->Synopsis</p></div>";
            if ($r->Etat == 'Disponible') {
                $page .= "<div class='etat Vert'>Disponibilité : $r->Etat</div>";
            }

            elseif ($r->Etat == 'Emprunté') {
                $page .= "<div class='etat Orange'>Disponibilité : $r->Etat</div>";
            }

            else {
                $page .= "<div class='etat Rouge'>Disponibilité : $r->Etat</div>";
            }

            $page .= "</div>";
        }


        return $page;
    }

}