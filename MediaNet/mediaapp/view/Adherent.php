<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 06/11/15
 * Time: 08:25
 */

namespace view;


class Adherent
{
    private $res, $type, $genre;
    private $emprunt;
    //Getter
    public function __get($attr_name)
    {
        if (property_exists(__CLASS__, $attr_name)) {
            return $this->$attr_name;
        }
        $emess = __CLASS__ . ": unknown member $attr_name (__get)";
        throw new \Exception($emess);
    }

    //Setter
    public function __set($attr_name, $attr_val)
    {
        if (property_exists(__CLASS__, $attr_name))
            $this->$attr_name = $attr_val;
        else {
            $emess = __CLASS__ . ": unknown member $attr_name (__set)";
            throw new \Exception($emess);
        }
    }

    public function afficher($action)
    {
        $html = "<!DOCTYPE html>
                    <html lang='fr'>
                <head>
                    <meta charset='UTF-8'>
                    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                    <link rel='stylesheet' type='text/css' href='mediaapp/ressources/css/stylesheets/styleAdherent.css'>
                    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
                    <script src='mediaapp/ressources/js/script.js'></script>

                    <title>MediaNet</title>
                </head>
                <body>

                <header>
                    <div class='icon' onClick='afficheMenu()'>
                        <div id='menu'>
                            <nav>
                                <ul>
                                    <li><a href='adherent.php?menu=recherche'>Recherche</a></li>
                                    <li><a href='adherent.php?menu=catalogue'>Catalogue</a></li>
                                    <li><a href='adherent.php?menu=connexion'>Connexion</a></li>
                                </ul>
                            </nav>
                        </div>
                        </div>
                    <h1 class='navtitre'>MediaNet</h1>
                </header>";

        switch ($action) {
            case 'recherche':
                $html .= $this->recherche();
                break;
            case 'resultats':
                $html .= $this->resultats();
                break;
            case 'afficheConnexion':
                $html .= $this->afficheConnexion();
                break;
            case 'empruntAdherent':
                $html .= $this->empruntAdherent();
                break;
            case 'page':
                $html .= $this->page();
                break;
        }

        $html .= "</body></html>";
        echo $html;
    }

    private function recherche()
    {
        $recherche = "<section>
                        <h1>Recherche</h1>

                        <form method='post' action='adherent.php'>
                            <input type='search' name='recherche' value ='' placeholder='Recherche....'>
                            <select name='type'>
                            <option value='type'>Type</option>";

        foreach ($this->type as $t) {
            $recherche .= "<option value=" . $t->TypeMedia . "> " . $t->TypeMedia . "</option >";
        }

        $recherche .= "</select>";
        $recherche .= "<select name='genre'>
                       <option value='genre'>Genre</option>";

        foreach ($this->genre as $g) {
            $recherche .= "<option value=" . $g->GenreDocument . "> " . $g->GenreDocument . "</option >";
        }

        $recherche .= "</select>";

        $recherche .= "<button class='btn' type='submit' name ='btnsearch'>Ok</button>
                        </form>
                    </section>";

        return $recherche;
    }

    private function resultats()
    {

        $resultat = "<section>

         <h1>Resultats</h1>";

        foreach ($this->res as $r) {

            $resultat .= "<div class = 'resultat'><a href='adherent.php?menu=resultat&document=$r->Id' >
                             <h2>$r->Titre</h2>

                             <img src =' $r->Image'/>

                             <div class = 'description'>
                                 <p>Auteur : $r->Auteur</p>
                                 <p>Date de publication : $r->Date</p>
                                 <p>Synopsis : $r->Synopsis</p></div>";

            if ($r->Etat == 'Disponible') {
                $resultat .= "<div class='etat Vert'>Disponibilité : $r->Etat</div>";
            }

            elseif ($r->Etat == 'Emprunté' || $r->Etat == 'Réservé') {
                $resultat .= "<div class='etat Orange'>Disponibilité : $r->Etat</div>";
            }

            else {
                $resultat .= "<div class='etat Rouge'>Disponibilité : $r->Etat</div>";
            }

            $resultat .= "</a></div>";
        }

        $resultat .= "</section>";
        return $resultat;
    }

    private function page(){
        $page = "<section>";
            foreach($this->res as $r){
                $page .= "<h1>$r->Titre</h1>
                        <div class = 'document'>


                             <img src =' $r->Image'/>

                             <div class = 'element'>
                                 <p>Auteur : $r->Auteur</p>
                                 <p>Date de publication : $r->Date</p></div>
                             <div class = 'synopsis'>
                                 <p>Synopsis : $r->Synopsis</p></div>";
                if ($r->Etat == 'Disponible') {
                    $page .= "<div class='etat Vert'>Disponibilité : $r->Etat</div>";
                }

                elseif ($r->Etat == 'Emprunté' || $r->Etat == 'Réservé') {
                    $page .= "<div class='etat Orange'>Disponibilité : $r->Etat</div>";
                }

                else {
                    $page .= "<div class='etat Rouge'>Disponibilité : $r->Etat</div>";
                }

                $page .= "</div>";
            }


        return $page;
    }

    private function afficheConnexion(){
        $recherche = "<section>
                        <h1>Connexion</h1>

                        <form method='post' action='adherent.php?menu=empruntAdherent'>
                            <input type='search' name='IdAdherent' placeholder='Id Adherent'>";
        $recherche .= "<button class='btn' type='submit' name ='btnConnexion'>Ok</button>
                        </form>
                    </section>";

        return $recherche;
    }

    private function empruntAdherent(){
        $affiche = '<section><div class ="docemprunt"> Document Emprunté :';
        foreach($this->emprunt as $e)
        {
            $affiche .= "<div>-".$e->Titre." (Date de retour : ".$e->DateRetour.")</div>";
        }
        $affiche .= '</div></section>';
        return $affiche;
    }
}