<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 04/11/15
 * Time: 22:06
 */

namespace model;


use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    public $timestamps = false;
    protected $table = 'Genre';
    protected $primaryKey = 'Id';

    public function Ajouter($genreDocument)
    {
        $genre = new Adherent();
        $genre->GenreDocument = $genreDocument;
        $genre->save();
    }

    public function Modifier($id, $genreDocument)
    {
        $genre = Genre::find($id);
        $genre->GenreDocument = $genreDocument;
        $genre->save();
    }

    public function Supprimer($id)
    {
        $genre = Genre::find($id);
        $genre->Delete();
    }

    public function afficheGenre()
    {
        $t = Genre::select('GenreDocument')
            ->orderBy('GenreDocument', 'ASC')
            ->get();

        return $t;
    }
}