<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 03/11/15
 * Time: 15:59
 */

namespace model;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public $timestamps = false;
    protected $table = 'Document';
    protected $primaryKey = 'Id';

    public function afficheDocument(){
        $doc = Document::select('*')
            ->get();
        return $doc;
    }

    public function recherche($rechercheTitre, $genre, $type)
    {
        // Si type et genre et titre
        if ($type != 'type' && $genre != 'genre' && $rechercheTitre != '') {
            $doc = Document::join('Type', 'Document.TypeId', '=', 'Type.Id')
                ->join('Genre', 'Document.GenreId', '=', 'Genre.Id')
                ->select('*')
                ->where('GenreDocument', '=', "$genre")
                ->where('TypeMedia', '=', "$type")
                ->where('Titre', 'LIKE', "%$rechercheTitre%")
//                ->where('Auteur', 'LIKE', "%$rechercheTitre%")
//                ->where('Synopsis', 'LIKE', "%$rechercheTitre%")
                ->get();
        } // Si type et genre
        elseif ($type != 'type' && $genre != 'genre') {
            $doc = Document::join('Type', 'Document.TypeId', '=', 'Type.Id')
                ->join('Genre', 'Document.GenreId', '=', 'Genre.Id')
                ->select('*')
                ->where('GenreDocument', '=', "$genre")
                ->where('TypeMedia', '=', "$type")
                ->get();
        } // Si type et titre
        elseif ($type != 'type' && $rechercheTitre != '') {
            $doc = Document::join('Type', 'Document.TypeId', '=', 'Type.Id')
                ->select('*')
                ->where('TypeMedia', '=', "$type")
                ->where('Titre', 'LIKE', "%$rechercheTitre%")
//                ->where('Auteur', 'LIKE', "%$rechercheTitre%")
//                ->where('Synopsis', 'LIKE', "%$rechercheTitre%")
                ->get();


        }// Si genre et titre
        elseif ($genre != 'genre' && $rechercheTitre != '') {
            $doc = Document::join('Genre', 'Document.GenreId', '=', 'Genre.Id')
                ->select('*')
                ->where('GenreDocument', '=', "$genre")
                ->where('Titre', 'LIKE', "%$rechercheTitre%")
//                ->orwhere('Auteur', 'LIKE', "%$rechercheTitre%")
//                ->orwhere('Synopsis', 'LIKE', "%$rechercheTitre%")
                ->get();
        }// Si type
        elseif ($type != 'type') {
            $doc = Document::join('Type', 'Document.TypeId', '=', 'Type.Id')
                ->select('*')
                ->where('TypeMedia', '=', "$type")
                ->get();

        } // Si genre
        elseif ($genre != 'genre') {
            $doc = Document::join('Genre', 'Document.GenreId', '=', 'Genre.Id')
                ->select('*')
                ->where('GenreDocument', '=', "$genre")
                ->get();

        }// Si titre
        else if ($rechercheTitre != '') {
            $doc = Document::select('*')
                ->where('Titre', 'LIKE', "%$rechercheTitre%")
//                ->where('Auteur', 'LIKE', "%$rechercheTitre%")
//                ->where('Synopsis', 'LIKE', "%$rechercheTitre%")
                ->get();
        } else {
            $doc = Document::select('*')
                ->get();
        }
        return $doc;
    }

    public function afficherDoc($id){
        $doc = Document::select('*')
            ->where('Id', '=', "$id")
            ->get();
        return $doc;
    }

    public function affichertype()
    {
        $g = Document::join('Type', 'Document.TypeId', '=', 'Type.Id')
            ->select('TypeMedia')
            ->get();
        return $g;
    }

    public function changerEtat($Id, $Etat)
    {
        $document = Document::find($Id);
        $document->Etat = $Etat;
        $document->save();
        return $document->Titre;
    }

    public function afficherTitre($Id)
    {
        $document = Document::find($Id);
        return $document->Titre;
    }
}