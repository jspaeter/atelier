<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 04/11/15
 * Time: 20:24
 */

namespace model;


use Illuminate\Database\Eloquent\Model;

class Emprunt extends Model
{
    public $timestamps = false;
    protected $table = 'Emprunt';
    protected $primaryKey = 'Id';

    public function emprunter($AdherentId, $DocumentId)
    {

        $emprunt = new Emprunt();
        $adherent = Adherent::find($AdherentId);
        $document = Document::find($DocumentId);

        $emprunt->DateEmprunt = date("Y-m-d");
        $emprunt->DateRetour = date("Y-m-d", strtotime('+15 days'));
        $emprunt->AdherentId = $adherent->Id;
        $emprunt->DocumentId = $document->Id;
        $emprunt->save();

        return $emprunt;
    }

    public function listeEmpruntParAdherent($AdherentId)
    {
        $emprunt = Emprunt::join('Document', 'Emprunt.DocumentId', '=', 'Document.Id')
            ->select('Titre', 'DateEmprunt', 'DateRetour')
            ->where('AdherentId', '=', "$AdherentId")
            ->get();

        return $emprunt;
    }

    public function RetourEmprunt($Id)
    {
        $emprunt = Emprunt::select('*')
            ->where('DocumentId', '=', $Id)
            ->get();
        $information = $emprunt[0]->AdherentId;
        $emprunt[0]->delete();
        return $information;
    }

    public function verificationEmprunt($DocumentId)
    {
        $emprunt = Emprunt::select('Id')
            ->where('DocumentId', '=', $DocumentId)
            ->get();

        return $emprunt;
    }
}