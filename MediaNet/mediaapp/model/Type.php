<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 04/11/15
 * Time: 09:15
 */

namespace model;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{

    public $timestamps = false;
    protected $table = 'Type';
    protected $primaryKey = 'Id';

    public function afficheType(){
        $g = Type::select('TypeMedia')
            ->orderBy('TypeMedia', 'ASC')
            ->get();

        return $g;
    }
}