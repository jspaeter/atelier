<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 04/11/15
 * Time: 17:26
 */

namespace model;


use Illuminate\Database\Eloquent\Model;

class Adherent extends Model
{
    public $timestamps = false;
    protected $table = 'Adherent';
    protected $primaryKey = 'Id';

    public function recupererAdherent($id){
        $adherent = Adherent::select('*')
            ->where('Id', '=', "$id")
            ->get();
        return $adherent;
    }

    public function ajouter($Nom, $Prenom, $Adresse, $Telephone, $Mail)
    {
        $emprunt = new Adherent();
        $emprunt->Nom = $Nom;
        $emprunt->Prenom = $Prenom;
        $emprunt->Adresse = $Adresse;
        $emprunt->Telephone = $Telephone;
        $emprunt->Mail = $Mail;
        $emprunt->save();
    }

    public function modifier($Id, $Nom, $Prenom, $Adresse, $Telephone, $Mail)
    {
        $user = Adherent::find($Id);
        $user->Nom = $Nom;
        $user->Prenom = $Prenom;
        $user->Adresse = $Adresse;
        $user->Telephone = $Telephone;
        $user->Mail = $Mail;
        $user->save();
    }

    public function supprimer($Id)
    {
        $user = Adherent::find($Id);
        $user->delete();
    }
}