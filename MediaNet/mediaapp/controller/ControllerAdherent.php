<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 04/11/15
 * Time: 09:50
 */

namespace controller;

use \model\Document;
use \model\Emprunt;
use model\Genre;
use model\Type;
use view\Adherent;
use view\catalogue;
use view\EmpruntView;
use view\Staff;
use view\View;

class ControllerAdherent
{

    //Getter.php
    public function __get($attr_name)
    {
        if (property_exists(__CLASS__, $attr_name)) {
            return $this->$attr_name;
        }
        $emess = __CLASS__ . ": unknown member $attr_name (__get)";
        throw new \Exception($emess);
    }

    // Setter
    public function __set($attr_name, $attr_val)
    {
        if (property_exists(__CLASS__, $attr_name))
            $this->$attr_name = $attr_val;
        else {
            $emess = __CLASS__ . ": unknown member $attr_name (__set)";
            throw new \Exception($emess);
        }
    }

    public function afficheRecherche()
    {
        // Création d'un modèle
        $doc = new Document();
        $type=  new Type();
        $genre= new Genre();

        $t = $type->afficheType();
        $g = $genre->afficheGenre();

        // Création d'une vue
        $vue = new Adherent();
        $vue->type = $t;
        $vue->genre = $g;
        // On utilise la fonction afficher
        $vue->afficher("recherche");
    }

    public function recherche($rechercheTitre, $genre, $type)
    {

        // Création d'un modèle
        $doc = new Document();

        // Récupération d'un doc
        $f = $doc->recherche($rechercheTitre, $genre, $type);

        // Création d'une vue
        $vue = new Adherent();

        // On met le document dans la vue
        $vue->res = $f;

        // On utilise la fonction afficher
        $vue->afficher("resultats");
    }

    public function catalogue()
    {
        // Création d'un modèle
        $doc = new Document();

        // Récupération d'un doc
        $f = $doc->afficheDocument();

        // Création d'une vue
        $vuecata = new Adherent();

        // On met le document dans la vue
        $vuecata->res = $f;

        // On utilise la fonction afficher
        $vuecata->afficher("resultats");
    }

//=========================================================================================
    public function afficherResultat($id){
        $d = new Document();
        $res = $d->afficherDoc($id);
        $v = new Adherent();
        $v->res = $res;
        $v->afficher('page');
    }

    public function afficherConnexion(){
        $vuecata = new Adherent();

        $vuecata->afficher("afficheConnexion");
    }

    public function afficherEmpruntAdherent(){
        $a = new \model\Adherent();
        $e = new Emprunt();

        $result = $a->recupererAdherent($_POST['IdAdherent']);
        if(count($result) == 1) {
            $emprunt = $e->listeEmpruntParAdherent($result[0]->Id);

            $viewEmprunt = new Adherent();
            $viewEmprunt->emprunt = $emprunt;
            $viewEmprunt->afficher('empruntAdherent');
        }
        else
        {
            $viewEmprunt = new Adherent();
            $viewEmprunt->afficher('afficheConnexion');
        }
    } //affiche les emprunts d'un adherent
}