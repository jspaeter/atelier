<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 04/11/15
 * Time: 09:50
 */

namespace controller;

use model\Adherent;
use \model\Document;
use \model\Emprunt;
use model\Genre;
use model\Type;
use view\Staff;

class ControllerStaff
{

    //Getter.php
    public function __get($attr_name)
    {
        if (property_exists(__CLASS__, $attr_name)) {
            return $this->$attr_name;
        }
        $emess = __CLASS__ . ": unknown member $attr_name (__get)";
        throw new \Exception($emess);
    }

    // Setter
    public function __set($attr_name, $attr_val)
    {
        if (property_exists(__CLASS__, $attr_name))
            $this->$attr_name = $attr_val;
        else {
            $emess = __CLASS__ . ": unknown member $attr_name (__set)";
            throw new \Exception($emess);
        }
    }

    public function afficheRecherche()
    {
        // Création d'un modèle
        $doc = new Document();
        $type=  new Type();
        $genre= new Genre();

        $t = $type->afficheType();
        $g = $genre->afficheGenre();

        // Création d'une vue
        $vue = new Staff();
        $vue->type = $t;
        $vue->genre = $g;
        // On utilise la fonction afficher
        $vue->afficher("recherche");
    }

    public function recherche($rechercheTitre, $genre, $type)
    {

        // Création d'un modèle
        $doc = new Document();

        // Récupération d'un doc
        $f = $doc->recherche($rechercheTitre, $genre, $type);

        // Création d'une vue
        $vue = new Staff();

        // On met le document dans la vue
        $vue->res = $f;

        // On utilise la fonction afficher
        $vue->afficher("resultats");
    }

    public function catalogue()
    {

        // Création d'un modèle
        $doc = new Document();

        // Récupération d'un doc
        $f = $doc->afficherTout();

        // Création d'une vue
        $vuecata = new Staff();

        // On met le document dans la vue
        $vuecata->res = $f;

        // On utilise la fonction afficher
        $vuecata->afficher("resultats");
    }

    public function afficheDocument(){
        $doc = new Document();

        $r = $doc->afficheDocument();

        $vuecata = new Staff();

        // On met le document dans la vue
        $vuecata->res = $r;

        // On utilise la fonction afficher
        $vuecata->afficher("afficheDocument");
    }

    public function changerEtatDocument($id, $etat)
    {
        $document = new Document();
        $document->changerEtat($id, $etat);
        $r = $document->afficheDocument();
        $view = new Staff();
        $view->res = $r;
        $view->afficher('afficheDocument');
    }

    public function ajouterEmprunt($AdherendId, $DocumentId)
    {
        $adherentId = $AdherendId;
        $documentId = $DocumentId;
        $emprunt = new Emprunt();
        $document = new Document();
        $verificationDocument = $document->afficherDoc($documentId);
        if(count($verificationDocument) == 1) {
            $adherent = new Adherent();
            $verificationAdherent = $adherent->recupererAdherent($AdherendId);
            if(count($verificationAdherent) == 1) {
                $emprunt->emprunter($adherentId, $documentId);
                $document->changerEtat($DocumentId, "Emprunté");
            }
        }
//        $this->afficherAjoutEmprunt();
    }

    public function afficherAjoutEmprunt()
    {
        $vue = new Staff();
        $vue->afficher('ajouterEmprunt');
    }

    public function afficherSupprimerEmprunt()
    {
        $vue = new Staff();
        $vue->afficher('supprimerEmprunt');
    }

    public function supprimerEmprunt()
    {
        $documentId = $_POST['DocumentId'];
        $emprunt = new Emprunt();
        $document = new Document();
        $vue = new Staff();
        $verification = $emprunt->verificationEmprunt($documentId);
        if(count($verification) == 1) {
            $AdherentId = $emprunt->RetourEmprunt($documentId);
            $listeEmprunt = $emprunt->listeEmpruntParAdherent($AdherentId[0]);
            $retourEmprunt = $document->changerEtat($documentId, "Disponible");
            $vue->res = array($listeEmprunt, $retourEmprunt);
            $vue->afficher('retourEmprunt');
        }
        else
        {
            $vue->afficher('supprimerEmprunt');
        }
    }

    public function afficherResultat($id){
        $d = new Document();
        $res = $d->afficherDoc($id);
        $v = new Staff();
        $v->res = $res;
        $v->afficher('page');
    }

    public function formulaireAdherent()
    {
        $vue = new Staff();
        $vue->afficher('formulaireAdherent');
    }

    public function ajouterAdherent()
    {
        $nom = $_POST['Nom'];
        $prenom = $_POST['Prenom'];
        $adresse = $_POST['Adresse'];
        $telephone = $_POST['Telephone'];
        $mail = $_POST['Mail'];
        $adherent = new Adherent();
        $adherent->ajouter($nom, $prenom, $adresse, $telephone, $mail);
        $view = new Staff();
        $view->afficher('formulaireAdherent');
    }

}