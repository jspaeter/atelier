<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 03/11/15
 * Time: 15:35
 */

require_once 'mediaapp/vendor/autoload.php';

app\App::DbConf('mediaapp/utils/config.ini');

if (isset($_POST['btnsearch'])) {
    $search = $_POST['recherche'];
    $searchtype = $_POST['type'];
    $searchgenre = $_POST['genre'];

    $ctl = new controller\ControllerAdherent();
    $ctl->recherche($search, $searchgenre, $searchtype);
}

elseif (isset($_POST['recherche'])) {
    $ctl = new controller\ControllerAdherent();
    $ctl->afficheRecherche();
}

elseif (isset($_POST['btnConnexion'])) {
    $ctl = new controller\ControllerAdherent();
    $ctl->afficherEmpruntAdherent();
}

elseif (isset($_GET['menu'])) {
    $search = $_GET['menu'];

    switch($search){
        case 'catalogue':
            $ctl = new controller\ControllerAdherent();
            $ctl->catalogue();
            break;
        case 'recherche':
            $ctl = new controller\ControllerAdherent();
            $ctl->afficheRecherche();
            break;
        case 'connexion':
            $ctl = new controller\ControllerAdherent();
            $ctl->afficherConnexion();
            break;
        case 'resultat':
            $ctl = new controller\ControllerAdherent();
            $ctl->afficherResultat($_GET['document']);
            break;
    }
}

else {
    $ctl = new controller\ControllerAdherent();
    $ctl->afficheRecherche();
}
